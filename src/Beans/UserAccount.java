package Beans;

public class UserAccount {
private String name,father,mother,adr,gender,dob,email;
long amount,phone;

public long getAmount() {
	return amount;
}

public void setAmount(long amount) {
	this.amount = amount;
}

public String getFather() {
	return father;
}

public void setFather(String father) {
	this.father = father;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getMother() {
	return mother;
}

public void setother(String mother) {
	this.mother = mother;
}

public String getAdr() {
	return adr;
}

public void setAdr(String adr) {
	this.adr = adr;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public String getDob() {
	return dob;
}

public void setDob(String dob) {
	this.dob = dob;
}

public long getPhone() {
	return phone;
}

public void setPhone(long phone) {
	this.phone = phone;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

}

