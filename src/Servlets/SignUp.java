package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.User;
import Dao.UserDao;

@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	PrintWriter out=response.getWriter();
	response.setContentType("text/html");
	String fname=request.getParameter("fname");
	String lname=request.getParameter("lname");
	String name=fname+" "+lname;
	String email=request.getParameter("email");
	String pwd=request.getParameter("pwd");
	String cont=request.getParameter("contact");
	long contact=Long.parseLong(cont);
	String gender=request.getParameter("gender");
	User user=new User();
	user.setName(name);
	user.setEmail(email);
	user.setPwd(pwd);
	user.setContact(contact);
	user.setGender(gender);
	UserDao ud=new UserDao();
	try {
		int i=ud.registerUser(user);
		if(i>0) {
			request.getRequestDispatcher("index.jsp").include(request, response);
			out.println("('Registered Sucessfully')");
		}
		else {
			request.getRequestDispatcher("registration.jsp").include(request, response);
			out.println("('Already Registered')");
		}
	} catch (ClassNotFoundException | SQLException e) {
		out.println(e);
	}
	}

}

