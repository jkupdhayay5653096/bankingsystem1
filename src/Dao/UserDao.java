package Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Beans.User;

public class UserDao {
	public Connection con;

	public Connection getCon() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/banking", "root", "root");
	}

	public int registerUser(User u) throws ClassNotFoundException, SQLException {
		con = getCon();
		String qr = "insert into registration values(?,?,?,?,?)";
		PreparedStatement ps = con.prepareStatement(qr);
		ps.setString(1, u.getName());
		ps.setString(2, u.getEmail());
		ps.setString(3, u.getPwd());
		ps.setLong(4, u.getContact());
		ps.setString(5, u.getGender());
		int i = ps.executeUpdate();
		return i;
	}

	public List<User> showUser() throws ClassNotFoundException, SQLException {
		con = getCon();
		String qr = "select * from accountant";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(qr);
		List<User> al = new ArrayList<User>();
		while (rs.next()) {
			User p = new User();
			p.setName(rs.getString("name"));
			p.setEmail(rs.getString("email"));
			p.setPwd(rs.getString("pwd"));
			p.setContact(rs.getLong("contact"));
			p.setGender(rs.getString("gender"));
			al.add(p);
		}
		return al;
	}

	public void close() throws SQLException {
		con.close();
	}
}

