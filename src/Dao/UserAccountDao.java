package Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import Beans.UserAccount;

public class UserAccountDao {
	public Connection con;

	public Connection getCon() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/banking", "root", "root");
	}

	public int addAccount(UserAccount ua) throws ClassNotFoundException, SQLException{
		con = getCon();
		String qr = "insert into accountTable values(?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = con.prepareStatement(qr);
		ps.setString(2,ua.getFather());
		ps.setString(1, ua.getName());
		ps.setString(3, ua.getMother());
		ps.setString(8, ua.getEmail());
		ps.setString(6, ua.getDob());
		ps.setString(4, ua.getAdr());
		ps.setLong(9,ua.getAmount());
		ps.setString(5, ua.getGender());
		ps.setLong(7, ua.getPhone());
		ps.setLong(10,(long) Math.random());
		int i=ps.executeUpdate();
		con.close();
		return i;
		
	}
}



