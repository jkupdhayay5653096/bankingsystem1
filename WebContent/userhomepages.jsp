<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<body>

<% String email=(String)session.getAttribute("userName");%>
<nav class="navbar navbar-expand-lg navbar-success bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">APNA BANK</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="openaccount.jsp">Open Account</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="balancequery.jsp">Balance</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="depositquery.jsp">Deposit</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="withdrawquery.jsp">Withdraw</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="transferMoney.jsp">Transfer</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="close.jsp">Close Account</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="creditcard.jsp">Credit Card</a>
        </li>
      </ul>
      <form class="d-flex">
     <a class="nav-link" href="Logout">Logout</a>
      </form>
    </div>
  </div>
</nav>
<center><h3><%="welcome "+email%></h3></center>

</body>
</html>
