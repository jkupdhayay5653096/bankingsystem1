<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student form</title>
<style>
.div {
	width: 800px;
	margin-top: 15px;
	margin-left: 300px;
	padding: 30px;
	background-color: black;
	color:white;
}

div {
	padding: 50px;
}

table {
	width: 600px;
	padding: 30px;
	align: center;
}
</style>

</head>
<body>
	    <center><h1>BANK OF INDIA</h1></center>
	<div class="div">
		<h1 align="center">REGISTRATION FORM</h1>
		<form action="addUser.jsp" method="post">
			<fieldset>
				<legend>Registration form</legend>
				<table>
					<tr>
						<td>USER NAME : *</td>
						<td><input type="text" name="uname" placeholder="First name"
							autofocus required /></td>
					</tr>
					<tr>
						<td>EMAIL:*</td>
						<td><input type="email" name="email"
							placeholder="Example@gmail.com" required></td>
					</tr>
					<tr>
						<td>PASSWORD:*</td>
						<td><input type="password" name="password" required></td>
					</tr>
					<tr>
						<td>CONTACT</td>
						<td><input type="text" name="contact" max-length=10 min-length=10></td>
					</tr>
					<tr>
						<td>GENDER</td>
						<td><select name="gender">
							<option>select gender</option>
							<option name="gender" value="MALE">MALE</option>
							<option name="gender" value="FEMALE">FEMALE</option>
							<option></option>
						</select></td>
					</tr>
				</table>
				<input type="checkbox" required> I agree to fill my
				details.All information given by me is correct.<br />
				<br /> <input type="submit" value="Register" name="submit" />
			</fieldset>
		</form>
	</div>

</body>
</html>