<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Open</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous"></script>



</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container-fluid">
		<a class="navbar-brand" href="#">Apna BANK</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
			data-bs-target="#navbarScroll" aria-controls="navbarScroll"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarScroll">
			<ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll"
				style="-bs-scroll-height: 100px;">
				<li class="nav-item"><a class="nav-link active"
					aria-current="page" href="#">Home</a></li>
				<li class="nav-item"><a class="nav-link" href="openaccount.jsp">Open
						Account</a></li>
				<li class="nav-item"><a class="nav-link"
					href="balancequery.jsp">Balance</a></li>
				<li class="nav-item"><a class="nav-link"
					href="depositquery.jsp">Deposit</a></li>
				<li class="nav-item"><a class="nav-link"
					href="withdrawquery.jsp">Withdraw</a></li>
				<li class="nav-item"><a class="nav-link"
					href="transferMoney.jsp">Transfer</a></li>
				<li class="nav-item"><a class="nav-link" href="close.jsp">Close
						Account</a></li>
				<li class="nav-item"><a class="nav-link" href="creditCard.jsp">Credit
						Card</a></li>
			</ul>
			<form class="d-flex">
				<a class="nav-link" href="Logout">Logout</a>
			</form>
		</div>
	</div>
	</nav>

	<section class="h-100 bg-dark">

	<div class="container  h-100">
		<div
			class="row d-flex justify-content-center align-items-center h-100">

			<div class="col">
				<div class="card card-registration my-4">
					<div class="row g-0">
						<div class="col-xl-6 d-none d-xl-block">
							<img
								src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/img4.webp"
								alt="Sample photo" class="img-fluid"
								style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;" />
						</div>
						<div class="col-xl-6">

							<div class="card-body p-md-5 text-black">
								<h3 class="mb-5 text-uppercase">Account registration form</h3>

								<div class="row">
									<form action="accountChecking.jsp">
										<div class="mb-3">
											<label for="name" class="form-label">Name </label> <input
												type="text" name="name" class="form-control" id="n1"
												aria-describedby="nameHelp">
										</div>
										<div class="mb-3">
											<label for="father" class="form-label">Father Name </label> <input
												type="text" name="father" class="form-control" id="f2"
												aria-describedby="Fathername">
										</div>
										<div class="mb-3">
											<label for="mother" class="form-label">Mother Name </label> <input
												type="text" name="mother" class="form-control" id="m1"
												aria-describedby="mother">
										</div>
										<div class="mb-3">
											<label for="address" class="form-label">Address</label> <input
												type="text" class="form-control" name="adr" id="a1">
										</div>
										<div class="mb-3">
											<label for="email" class="form-label">Email </label> <input
												type="email" name="email" class="form-control" id="e1"
												aria-describedby="emailHelp">
										</div>
										<div class="mb-3">
											<label for="amount" class="form-label">Amount </label> <input
												type="number" name="amount" class="form-control" id="am1"
												aria-describedby="amtHelp">
										</div>
										<div class="mb-3">
											<label for="gender" class="form-label">Gender</label> <select
												name="gender"
												class="form-control browser-default custom-select">
												<option name="gender" value="M">Male</option>
												<option name="gender" value="F">Female</option>
												<option name="gender" value="O">Unspecified</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="DOB" class="form-label">DOB</label> <input
												type="text" class="form-control" placeholder="DD/MM/YY" name="dob" id="p1">
										</div>

										<div class="mb-3">
											<label for="phone" class="form-label">Phone</label> <input
												type="number" class="form-control" name="phone" id="pp1">
										</div>
										<div class="mb-3 form-check">
											<input type="checkbox" class="form-check-input"
												id="exampleCheck1"> <label class="form-check-label"
												for="exampleCheck1">I agree to all Terms &
												condition.</label>
										</div>
										<button type="submit" class="btn btn-primary">Submit</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
